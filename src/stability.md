---
title: website stability note
layout: blank
head: >
  <style>
    body {
      font-family: monospace;
      font-size: 1.1rem;
      max-width: 65ch;
      margin: 1em;
    }
  </style>
---

This website (everything residing on "geheimesite.nl" and previously "roblog.nl", excluding subdomains) is volatile. It can, and will, change. [My URLs aren't cool](//www.w3.org/Provider/Style/URI), sorry! Attempts to link to this site are futile.

If you do want to link to something and be confident that the link won’t die, please link to the appropriate page in [The Archives](//classic.geheimesite.nl). I am committed to keeping the URL schemes of classic.geheimesite.nl and blog.geheimesite.nl stable.

Note: the path to the RSS feed will always stay <a href="/index.xml">/index.xml</a>.
