---
title: Colophon
---

{% container %}
<h1>{{ title }}</h1>

This site is built using [Eleventy](//11ty.dev), the simpler static site generator. The generated HTML is being hosted over at [bHosted](//bhosted.nl), here in the Netherlands.

The source code is available on [GitHub](//github.com/RobinBoers/geheimesite.nl), [Codeberg](//codeberg.org/RobinBoers/geheimesite.nl) and [Gitz](//git.dupunkto.org/~axcelott/geheimesite.nl), and is licensed AGPLv3 because I strongly believe in Copyleft.

The site primarily uses your system font stack. All other fonts (on other subdomains) are self-hosted [to prevent Google from tracking my visitors](//brycewray.com/posts/2020/08/google-fonts-privacy/).

All markdown written in Neovim, the perfect hammer for an imperfect world. Best viewed in any browser, including Firefox, Netscape Navigator, Internet Explorer and lynx.
{% endcontainer %}
