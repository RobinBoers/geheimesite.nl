---
title: Projects
---

{% container %}

<h1>{{title}}</h1>

I build web-based applications and games in Elixir and PHP, using a little too much JavaScript. Everything I build is open-sourced on [Gitz](//git.dupunkto.org).

Here's some projects I'm especially proud of:

- [Signo](//git.dupunkto.org/~axcelott/signo)&nbsp; Experimental compiler for a Lisp-inspired language, written in Elixir.

- [Untitled](//git.dupunkto.org/~dupunkto/pubb)&nbsp; Explores ways to blend websites, social media and collaboration on the Web, by implementing an easy-to-use website engine powered by various open standards.

- [Oblikvo](//git.dupunkto.org/~dupunkto/oblikvo)&nbsp; Browser-based 3D first-person shooter with old school graphics and multiplayer support.

{% hr %}

## Webdesign

I build websites for people from time to time. Here’s a few of them. All of the people were happy. I think.

{% endcontainer %}

<ul class="grid">
  <li>
    <h3>HVM Old's Cool</h3>
    <a href="//hvm-oldscool.nl">
      <img src="https://cdn.geheimesite.nl/images/websites/hvm-oldscool.png" alt="">
    </a>
  </li>

  <li>
    <h3>De Groentemeester</h3>
    <a href="//degroentemeester.nl">
      <img src="https://cdn.geheimesite.nl/images/websites/degroentemeester.png" alt="">
    </a>
  </li>

  <li>
    <h3>Sterrenkruid</h3>
    <a href="//jokessterrenkruid.blogspot.com">
      <img src="https://cdn.geheimesite.nl/images/websites/sterrenkruid.png" alt="">
    </a>
  </li>

  <li>
    <h3>Streekproeverij Weij</h3>
    <a href="//weijstreekproeverij.nl">
      <img src="https://cdn.geheimesite.nl/images/websites/streekproeverij-weij.png" alt="">
    </a>
  </li>

  <li>
    <h3>Power Radio 010</h3>
    <a href="//powerradio010.nl">
      <img src="https://cdn.geheimesite.nl/images/websites/powerradio010.png" alt="">
    </a>
  </li>

  <li>
    <h3>Dagboek van een Grootmoeder</h3>
    <a href="//grootmoedersdagboek.blogspot.com">
      <img src="https://cdn.geheimesite.nl/images/websites/grootmoedersdagboek.png" alt="">
    </a>
  </li>

  <li>
    <h3>Sharefox</h3>
    <a href="//sharefox.eu">
      <img src="https://cdn.geheimesite.nl/images/websites/sharefox.png" alt="">
    </a>
  </li>
</ul>
