---
title: Resumé
---

<article>
  <h1>Robin Boers</h1>

  <p>
    <strong>Elixir Developer & webdesigner</strong><br>
    <a href="/contact">hello@geheimesite.nl</a>
  </p>

  <h2>Technical skills</h2>

  <p>
    I'm pretty good at building websites. I know HTML/CSS, obviously. I'm
    also fluent in Elixir and JavaScript, amd I know a little PHP and Python
    too. I generally prefer high-level functional programming languages
    without mutable references.
  </p>

  <p>
    I'm comfortable on the UNIX commandline and with Git version control. I
    also have experience with various flavours of SQL (mainly PostgresSQL at
    work and MySQL for hobby projects) and a few styling frameworks, of
    which I liked TailwindCSS the best. I know a little Docker, but trust
    me, you don't want me to deploy production-critical software without
    supervision.
  </p>

  <p>
    Overall, while I do have my own preferences and opinions, I'm capable of
    picking up any technology and going with it.
  </p>

  <p>
    And yes, I can work with WordPress, but I'd rather use a
    <a href="//en.wikipedia.org/wiki/Static_site_generator">
      Static Site Generator.
    </a>
  </p>

  <h2>Experience</h2>

  <table>
    <thead>
      <tr>
        <th>Title</th>
        <th>Organisation</th>
        <th>Duration</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Co-founder &amp; maintainer</td>
        <td>{du}punkto</td>
        <td>2023-Current</td>
      </tr>
      <tr>
        <td>Junior Developer</td>
        <td>Qdentity</td>
        <td>2021-Current</td>
      </tr>
    </tbody>
  </table>

  <p>
    At Qdentity, I worked with Elixir, Phoenix LiveView and TailwindCSS and
    I built <a href="//sharefox.eu">Sharefox</a> along with some other
    internal APIs. Later, I also worked extensively on major parts and/or
    features of their CMS.
  </p>

  <p>
    I also built <a href="webdesign.html">websites</a> as independent
    webdesigner for various people throughout the years.
  </p>

  <h2>Certifications</h2>

  <table>
    <thead>
      <tr>
        <th>School</th>
        <th>Program</th>
        <th>Duration</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Lyceum Schravenlant</td>
        <td>VWO/Atheneum</td>
        <td>2019-2025</td>
      </tr>
    </tbody>
  </table>

  <p style="margin-top: 0.2em">
    <small>Yup, that's not a lot, I know :)</small>
  </p>

  <p>
    I'm still in consideration about what study I will do after high school.
    Feel free to email me if you have suggestions.
  </p>

  <h2>Languages</h2>

  <p>
    I speak both Dutch, which is my native language, as well as English,
    which I learned through online interactions and a lot of Netflix (and I
    also got courses at school of course ;)
  </p>

  <p>
    My English is fairly decent in terms of grammar and general fluency,
    however, I find my vocabulary to be disappointingly narrow, as it is
    primarily computer-focused. So basically, I can talk for hours about
    anything related to UNIX, networking, cryptography, mathmatics, or
    related subjects, but I'll struggle to formulate even a single sentence
    if you ask me about my holiday.
  </p>

  <h2>Projects & Interests</h2>

  <ul>
    <li>Free software</li>
    <li>Alternatives to the Web</li>
    <li>Protocols</li>
    <li>Functional programming</li>
    <li>*NIX systems</li>
    <li>Webdesign & UI design</li>
    <li>Documentation</li>
    <li>Writing</li>
  </ul>
</article>
