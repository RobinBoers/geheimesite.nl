---
title: Buttons
head: >
  <style>
    .container {
      max-width: 800px;
    }

    img {
      width: 88px;
      height: 31px;
    }
  </style>
---

{% container %}

[![Button](https://cdn.geheimesite.nl/images/buttons/valid-html.gif)](//validator.w3.org/nu/?doc=https%3A%2F%2Froblog.nl)
[![Button](https://cdn.geheimesite.nl/images/buttons/valid-css.gif)](//jigsaw.w3.org/css-validator/check/referer)
![Button](https://cdn.geheimesite.nl/images/buttons/expectations.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/invalidator.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/valid-rss.png)](//validator.w3.org/feed/check.cgi?url=https%3A%2F%2Froblog.nl%2Findex.xml)
[![Button](https://cdn.geheimesite.nl/images/buttons/valid-atom.png)](//validator.w3.org/feed/check.cgi?url=https%3A%2F%2Froblog.nl%2Fatom.xml)
[![Button](https://cdn.geheimesite.nl/images/buttons/choose-mozilla.gif)](//www.jwz.org/blog/2016/10/they-live-and-the-secret-history-of-the-mozilla-logo/)
[![Button](https://cdn.geheimesite.nl/images/buttons/duckduckgo.gif)](//duckduckgo.com)
![Button](https://cdn.geheimesite.nl/images/buttons/internet-exploder.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/mozilla-old.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/best-viewed-with-eyes.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/clickhere.gif)](//yewtu.be/watch?v=dQw4w9WgXcQ)
![Button](https://cdn.geheimesite.nl/images/buttons/firefox.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/github-pages.gif)](//pages.github.com)
[![Button](https://cdn.geheimesite.nl/images/buttons/agpl.gif)](//gnu.org/licenses/agpl-3.0.en.html)
[![Button](https://cdn.geheimesite.nl/images/buttons/gpl.gif)](//gnu.org/licenses/gpl-3.0.en.html)
[![Button](https://cdn.geheimesite.nl/images/buttons/fuckweb3.gif)](//yesterweb.org/no-to-web3/)
[![Button](https://cdn.geheimesite.nl/images/buttons/nft.gif)](//www.stephendiehl.com/blog.html)
[![Button](https://cdn.geheimesite.nl/images/buttons/mozilla.gif)](//drewdevault.com/2016/05/11/In-Memoriam-Mozilla.html)
[![Button](https://cdn.geheimesite.nl/images/buttons/neocities-now.gif)](//neocities.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/gutenberg.gif)](//gutenberg.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/minecraft.gif)](//minecraft.net)
![Button](https://cdn.geheimesite.nl/images/buttons/netscape-now.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/fuckchrome.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/mastodon.gif)](//joinmastodon.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/latex.gif)](//latexeditor.lagrida.com)
![Button](https://cdn.geheimesite.nl/images/buttons/built-with-notepad.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/gimp.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/fuckfacebook.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/dontclickhere.gif)](http://hat.net/abs/noclick/)
[![Button](https://cdn.geheimesite.nl/images/buttons/indieweb-now.gif)](//indieweb.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/no-cookies.gif)](/v/19/colophon)
[![Button](https://cdn.geheimesite.nl/images/buttons/theoldnet.gif)](//theoldnet.com)
[![Button](https://cdn.geheimesite.nl/images/buttons/neocities.gif)](//neocities.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/indieweb.png)](//indieweb.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/webmention.png)](//webmention.net)
[![Button](https://cdn.geheimesite.nl/images/buttons/microformats.png)](//microformats.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/webmentions.gif)](/v/19/colophon)
![Button](https://cdn.geheimesite.nl/images/buttons/linux.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/rss.gif)](/v/19/rss)
![Button](https://cdn.geheimesite.nl/images/buttons/powered-by-freebsd.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/humanmade.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/bad-hair-days.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/16bit.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/piracy.gif)
[![Button](https://cdn.geheimesite.nl/images/buttons/lynx-now.gif)](//lynx.browser.org)
![Button](https://cdn.geheimesite.nl/images/buttons/dumbass.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/design.png)
![Button](https://cdn.geheimesite.nl/images/buttons/queer.png)
[![Button](https://cdn.geheimesite.nl/images/buttons/sites/satyrs.png)](//satyrs.eu)
[![Button](https://cdn.geheimesite.nl/images/buttons/sites/devastatia.png)](//devastatia.com)
[![Button](https://cdn.geheimesite.nl/images/buttons/sites/foreverlikethis.png)](https://foreverliketh.is)
[![Button](https://cdn.geheimesite.nl/images/buttons/sites/mayaland.png)](//maya.land)
[![Button](https://cdn.geheimesite.nl/images/buttons/sites/degenerate.png)](//degenerates.neocities.org)
[![Button](https://cdn.geheimesite.nl/images/buttons/yesterweb.png)](//suboptimalism.neocities.org/writings/yesterweb)
[![Button](https://cdn.geheimesite.nl/images/buttons/sites/sadgrl.gif)](//sadgrl.online)
[![Button](https://cdn.geheimesite.nl/images/buttons/wiby.gif)](//wiby.me)
![Button](https://cdn.geheimesite.nl/images/buttons/reddit.gif)
![Button](https://cdn.geheimesite.nl/images/buttons/fuckai.png)
[![Button](https://cdn.geheimesite.nl/images/buttons/indieseek.png)](//indieseek.xyz)

![Button](https://cdn.geheimesite.nl/images/buttons/small/cc0.png)
![Button](https://cdn.geheimesite.nl/images/buttons/small/indieweb.webp)
![Button](https://cdn.geheimesite.nl/images/buttons/small/microformats.webp)
![Button](https://cdn.geheimesite.nl/images/buttons/small/webmention.png)
![Button](https://cdn.geheimesite.nl/images/buttons/small/anybrowser.png)
![Button](https://cdn.geheimesite.nl/images/buttons/small/valid-html5.png)
![Button](https://cdn.geheimesite.nl/images/buttons/small/valid-rss2.png)
![Button](https://cdn.geheimesite.nl/images/buttons/small/valid-css.png)

Here's my button:  
<img eleventy:ignore src="/button.jpeg" alt="Button">

{% endcontainer %}
