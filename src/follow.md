---
title: Follow me
head: > 
  <style>
    hgroup p { font-size: 0.8em }
  </style>
---

{% container %}
<hgroup>
  <h1>{{ title }}</h1>
  <p>(If you already know RSS, <a href="/index.xml">here's my feed</a>.)</p>
</hgroup>

I don’t like social media. Actually, [I kinda hate it to be honest](/blog/social-media-wat-moeten-we-er-mee-aan). Don't be sad tho! Instead, I have a few [RSS](/rss) feeds.

TL;DR RSS is an ancient internet protocol for syndicating creative work to audiences. Copy-pasting the link to this website into your reader should work. If not, you can try these links:

- [Subscribe via RSS](/index.xml)
- [Subscribe via Atom](/atom.xml)
- [Subscribe via JSON](/feed.json)

{% endcontainer %}
