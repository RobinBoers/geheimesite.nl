---
title: Rules
date: 2024-03-21
---

[In the past](//blog.geheimesite.nl/notes/regels/) I've written about what I called "two rules for any well-functioning group":

> - Everyone behaves
> - Everyone should have fun

However, I was probably wrong. Because while small groups of people are pretty much self-organizing, as the group grows, some hierarchy is needed. In a relatively small group--everyone knows everyone--rules are generally unneeded, as people tend to behave properly due to peer pressure and social dynamics. However, the controlling effect of peer pressure breaks down in larger groups, because people are relatively anonymous. Social dynamics alone are not effective anymore, hence the need for formal rules.

So while you can get away with my two rules for a small group, let's say a classroom of around ~25 people, they will most definitely cause complete anarchy and chaos when applied to the entire school of around 700 students.

I was right about one thing tho:

> If there is no basis for the rule, the rule doesn't work, or the rule is unneeded, it should probably not be a rule.
