---
title: The role of generative AI in writing
date: 2024-01-23
favorite: true
---

Since its introduction a little more than a year ago, we've been discussing ChatGPT a lot at school. It's an interesting topic, for sure. How can it be used in school? By students or teachers? Is using AI cheating? Will it interfere with the development of writing skills or help? Should it be used at all?

Generally, we seem to have the urge to ban upcoming technologies that radically change things. Indeed, I've repeatedly heard people call to ban use of generative AI, calling it plagiarism and fraud. However, like everything, the matter is more nuanced than seems to be the case at first sight. Even if we were to completely ban generative AI, enforcing a ban like that will prove harder and harder the further models like GPT evolve. Besides, it probably won't restrain students from using models like them. Therefore, we should think about and discuss how we could properly integrate AI models into our writing.

With that in mind, here are some of the primary takeaways after using ChatGPT on-and-off for about a year.

ChatGPT is--mistakenly--mainly used to *generate* prose. However, the prose it generates is mediocre; it's dull and repetitive. Moreover,--even tho OpenAI tries to prevent it--the model hallucinates fairly often. This means nothing it generates can be trusted to be factually correct.

Instead, it is way more powerful as a tool to edit and transform language. As Linus wrote a while back:

<blockquote cite="//thesephist.com/posts/latent/">
  <p>
    Not "computers can complete text prompts, now what?" but
    "computers can understand language, now what?"
  </p>
</blockquote>

The AI models are unable to understand what they are writing about. They're only selecting words that sound plausible given the context of their training data and the prompt. But they do know grammar, spelling, and essay structure. That's where their strength lies. Not the ability to generate okay-sounding essays, but their ability to understand and transform language. Use it to your advantage.

Instead of prompting "can you generate an argumentative essay on the role of religion in public schools", we should prompt things like "can you rewrite this paragraph to be more academic" or "can you show me alternatives to this sentence".

ChatGPT and similar models are not a threat to writing. Instead, they are merely new tools that can--if utilized correctly--further improve our writing. The impact they will have is purely defined by how we use them.

<small>*this post was written by me, but edited and <a href="//languagetool.org">checked for grammar mistakes</a> using AI.</small>

<details>
  <summary>Extras</summary>
  <p>Some additional notes:</p>
  <ul>
    <li>
      LLMs are best used in combination with existing tools, like
      <a href="//jsomers.net/blog/dictionary">a good dictionary</a>,
      thesaurus, and spell checkers.
    </li>
    <li>
      It's really a bit silly to call this category of AI
      "generative", because (in my opinion) that's the
      thing they're worst at.
    </li>
    <li>
      While not really related to this topic, the
      <a href="//thesephist.com/posts/latent/"
        >post I quoted from Linus</a
      >
      is great and you should definitely read it!
    </li>
  </ul>
</details>
