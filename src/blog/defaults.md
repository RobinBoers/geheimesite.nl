---
title: Good Defaults
date: 2024-03-15
---

Most documents are written in Calibri or Arial, everyone uses Google, and links on the second page of Google are rarely seen. [Only 5% of users actually change the settings](//archive.uie.com/brainsparks/2011/09/14/do-users-change-their-settings/).

This is the [Default Effect](//en.wikipedia.org/wiki/Default_effect)--people don't change the defaults. Therefore, whatever ships with your product, that's what most users will use.

An potential explanation would be [decision fatigue](//en.wikipedia.org/wiki/Decision_fatigue): a psychological phenomenon in which a person's ability to make decisions deteriorates after making many decisions. Constantly making decisions is exhausting. Thus, when people are faced with multiple choices, they tend to stick to the default ones.

<blockquote
  cite="https://bootcamp.uxdesign.cc/why-default-settings-matter-more-than-you-think-97af8c785edf">
  <p>
    People change the defaults if the effort required to change them
    feels less than the annoyance of sticking with them.
  </p>
</blockquote>

<!-- Settings are complicated-and scary. The defaults work. Modifying them only poses the risk of breaking *something somewhere*, with no clue how to fix it. -->

Furthermore, users assume us programmers to know better. And frankly, we should. Just because we nerds like to play around with the settings doesn't mean everyone does. And it shouldn't be necessary either. Thus, the takeaway is: provide good defaults.
