---
title: Surf, don't search
date: 2024-05-29
---

You're using the internet wrong. That's quite a statement, isn't it?

<!--
  A few months ago, I introduced my sister to [RSS](/rss).
  She seemed to like the idea, but she kept asking one question, over
  and over. "How do I find things to follow?"

  Excellent question! How *do* you find interesting things on the
  Web? The anwser? Browse it; follow links; surf around.
-->

The power of the Web is its hyperlinked nature. It's a *web*, not just a repository. Yet we're increasingly *searching* everything on monolithic search engines like Google. That's bad. For one, it limits you to a very small portion of the Web--and it's not [the best part either](/blog/love-hate).

But also: our brain works by association:

<blockquote cite="//thesephist.com/posts/nav/">
  <p>
    When I get stuck on a really hard problem, whether it's some
    impossible bug in my code or my sofa not fitting through my front
    door on moving day, I close my eyes and … think really hard.
    Somewhere behind my shut eyelids and confused eyeballs, things are
    happening. Electricity is flowing through the vat of brain-stuff and
    spindly wires that somehow make up my thought process, and for a few
    seconds, they just kind of do their thing. Until, if I'm lucky, an
    answer pops into my head a few moments later.
  </p>

  <p>
    One of my favorite questions to ponder these days has been: when I'm
    thinking or remembering, just in that moment when my eyes are closed
    and I'm sending all that extra energy to my brain, what's really
    happening? There are no hard drives to spin into place in my brain.
    What's taking up all that time? It's easy to wave your hands and say
    “it's just computing” or whatever, the way you expect slow computers
    under load to be. But often when I'm heads-down thinking, I'm not
    crunching numbers or solving logic puzzles in my head. I'm not
    really sure what I'm doing, but it usually feels like just staring
    into the void and hoping for some idea to pop into my mind.
  </p>

  <blockquote
    cite="//theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/">
    <p>
      The human mind [...] operates by association. With one item in its
      grasp, it snaps instantly to the next that is suggested by the
      association of thoughts, in accordance with some intricate web of
      trails carried by the cells of the brain. It has other
      characteristics, of course; trails that are not frequently
      followed are prone to fade, items are not fully permanent, memory
      is transitory. Yet the speed of action, the intricacy of trails,
      the detail of mental pictures, is awe-inspiring beyond all else in
      nature.
    </p>
  </blockquote>

  <p>
    If we imagine the repository of ideas and memories in a mind as a
    kind of tangled web of ideas, “thinking” definitely involves
    traversing and scrambling across this web somehow, with some intent.
    The more obvious, trivial thoughts are the associations that are
    immediate and close by, and the more insightful thoughts may be
    jumps between ideas that are only loosely connected, or only
    connected by second- or third-degree leaps in association.
  </p>
</blockquote>

The most valuable aspect of the Web is the ability to travese sources in this way. By using a search bar you're missing this crucial aspect of the Web.
