---
title: Thoughts on the mobile phone ban
date: 2024-01-08
---

Last month the Dutch government announced that from next year on mobile phones will be banned in high school. As a student who is currently in high school, I think it's a fundamentally good development, but the media attention around it just feels wrong.

The news is full of items of cherry-picked comments from teenagers who feel "relieved" because they "finally talk to each other". But also teenagers who are sad because "they won't have any memories of high school if they can't take photos". They present a greatly exaggerated story praising this measure the government imposed to protect the youth against themselves.

It's all so over the top. Contrary to popular belief, teenagers *do* talk to each other, even in the presence of a mobile  phone. In breaks, we play card games like Uno and Bullshit. We read books. We complain about how stupid school and homework is. You know, normal stuff. And our phones have a place too. Sometimes we play mobile games in the break, like [Evil Apples](//www.evilapples.com) or [Gartic Phone](//garticphone.com). But  that's still something we do *together*.

And, yes, phones are distracting. Yes, they cause mayhem from time to time. Yes, social media is shit. I am very much aware. But all of that is also true for adults. These devices and the apps on them are made to be extremely addictive, it's their fucking business model. It's not just teenagers who get addicted. And it's certainly not our fault that we get addicted. Even the people who built these apps in the first place, who know exactly how they work and what tricks are used to exploit our psychology, they still [aren't able to restrain themselves](//thesocialdilemma.com). Adults aren't magically immune.

It feels like it's a bunch of grey birds going "kids these days...", "you can't get off your phones, and now we're going to take them away", "it's for your own good!" and then continuing to celebrate on social media. Hypocrisy at its peak.

So yes, I think it's great that phones have been banned in high schools. The next step would be everywhere else (and especially the parlement).

<details>
  <summary>Extras</summary>
  <p>Some other notes:</p>
  <ul>
    <li>
      <p>
        Schools spent years digitizing everything. Schedules, grades,
        homework, study materials and more are now all exclusively
        available in web applications. They made this move with the
        assumption that these platforms would always be accessible for
        students via their mobile phone (a wrong assumption by the way,
        because this excludes students like myself that would rather
        leave their phone at home). How are schools going to deal with
        this?
      </p>
    </li>
    <li>
      <p>
        How are schools going to enforce this? They've been burdened
        with enough additional administrative tasks already. And what
        even counts as a mobile phone?
        <del>
          Do all mobile devices count? What about my iPad? Or an old
          iPod Touch? My DIY Raspberry Pi Zero project with a
          rectangular touch screen? And what if it were a square
          instead? And what about smartwatches? They can TikTok too.
          Does every portable device with an internet connection fall
          under this new law?
        </del>
      </p>
      <p>
        <ins>
          Update: I read the law on rijksoverheid.nl and they include
          "smartphones", "tablets", and "smartwatches".
          <em>However</em>, they do not provide a definition for these
          terms. Disappointing.
        </ins>
      </p>
    </li>
  </ul>
</details>
