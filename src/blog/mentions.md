---
title: '@mentions'
date: 2024-04-09
rssonly: true
---

I've implemented *@mentions* on my site, inspired by [@devastatia](//devastatia.com)'s site. This allows me to tag fellow Webmasters when I write something referring to their work, hopefully starting interesting conversations.
