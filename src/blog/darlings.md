---
title: Darlings
date: 2024-05-25
---

The saying goes "kill your darlings". When I have trouble fitting my writing into a cohesive whole, it's often because I've tangled up multiple concepts or ideas; essentially trying--and failing--to create a coherent two out of two, although related, separate concepts--leaving them both underdeveloped.

In those moments the best solution is to split up my text, to provide each idea the space and care it deserves.

So maybe don't kill the poor things. Just *viciously* pull them apart.
