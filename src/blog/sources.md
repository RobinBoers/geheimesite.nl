---
title: In favor of the small
date: 2024-03-18
---

Since the internet went mainstream in the 90s, information has been readily available for everyone. But can it be trusted? What makes a reliable source? At school, we seem to get taught 'the bigger the better'. Big news sites are generally considered trustworthy, while small and relatively unknown sites, like personal blogs[^1] or forums, are off-limits. However, in my experience, small, specialized blogs are typically better sources than those big news outlets.

For one, random people online usually aren't writing to please an advertiser, publisher or particular audience--they're writing because they're passionate about the subject. This makes a huge difference, as there is no financial incentive, no money at stake, no undisclosed incentives, no monetary gain. They don't need to clickbait their writing; there's no reason to maximize views. A blogger that isn't financially dependent on their blog has no goal to track you or sell ads. They don't *need anything* from their readers.

Furthermore, articles on credible news sites are generally written by journalists, which is good. They fact-check and remain objective and neutral. But, a journalist will always be a generalist: they know a little about a lot. Bloggers, on the other hand, can be specialists and will often have a more in-depth understanding of the topic at hand. In other words: specialized blogs are usually more accurate than media coverage on the same subject. You're probably [familiar with this too, if you've ever read some news item about your own profession](//rubenerd.com/erwin-knolls-law-of-media-accuracy/):

<blockquote
  cite="//rubenerd.com/erwin-knolls-law-of-media-accuracy/">
  <p>
    I've mentioned a few times how easy it is to pick apart inaccuracies
    in media coverage on a topic you know about, and the unease that
    sets in when you realise every topic likely has the same issues.
  </p>
</blockquote>

Moreover, bloggers don't have any restrictions on their writing: they can make their articles as long, technical, and detailed as they please, not having to tailor them to a specific audience. Whereas a journalist has to make their writing accessible to a larger audience, exchanging valuable detail for readability.

On blogs and forums, I know that what I'm reading is the authentic and personal experience of an actual person -- not some corporate marketing bullshit. I'm not alone in this btw; there's a reason people [keep appending "reddit" to the end of their searches](//dkb.blog/p/google-search-is-dying). Small blogs are written by *people*. I think that scares institutions like schools, which were created in a world where only other big institutions, like media companies and publishers, had the ability to put stuff into the world. "Oh no, actual people are now able to write and publish stuff on their own!" PANIC!

The advent of the internet caused a huge shift in the way we find and consume information. And we love change--if we're the one doing it. But for the rest of us, change is a threat. A threat to what is familiar. Familiar is nice. Familiar is comfortable. But that doesn't mean familiar is good. Yet our education system keeps desperately clinging onto what it finds familiar.

But what if we instead embrace the internet and the voices it gives to ordinary people? Maybe a more diverse and less formalised set of sources will actually be an enrichment for class. Offer us new perspectives we hadn't thought of before.

So, I think we should be more kind towards stuff written by people. It's not scary; it's just different.

[^1]: The blogs I talk about here are not the commercial BuzzFeed kind of blogs, but rather the things you'd find on the [IndieWeb](//indieweb.org) or [Small](//neustadt.fr/essays/the-small-web/) [Web](//www.marginalia.nu/marginalia-search/about/).
