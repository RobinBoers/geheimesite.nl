---
title: Banning phones is not solving anything
date: 2024-01-14
---

A few days ago, I posted some of my thoughts regarding the new mobile phone ban in high schools in the Netherlands. I want to add a few more things:

I think the fundamental problem is not the phones themselves. Why are phones distracting? Push notifications and FOMO. *The phones are not the problem, social media is*. And because we failed to properly regulate social media, phones are getting banned.

The ban is a prime example of symptom relief. The government is doing nothing to actually address the problem; all they're doing is prohibiting the manifestation. The ban will *not* help students overcome their addiction. Neither is the ban preventing social media use and all the problems than come with it; students are simply going to use different devices to access their beloved Snapchat selfies.
