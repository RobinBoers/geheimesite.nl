---
title: Movies and Limited Series
date: 2024-02-07
favorite: true
---

So lately I've been watching a lot of Netflix.

Generally, I'm not the kind of person that consumes a lot of media. I'd rather make things; I find creating things way more satisfying.

However, I got a bit burned out in terms of ideas--I didn't know what to build-- energy, and time (school am I right :sparkles:). So, instead I spent about 20 hours of my life staring at a screen, looking at fictional characters living their almost perfect made-up lives.

Anyways, I really enjoyed these in particular:

- After
- Tick, tick… BOOM!
- Heartstopper
- Inventing Anna
- Bohemian Rhapsody
- Love, Rosie
- Friends with Benefits
- Lady Chatterley's Lover
- The Girl Nextdoor
- Queen's Gambit
- Don't Look Up
- Dirty Lines
- The Social Network
- The Playlist
- The Social Dilemma
- Yesterday
- I Used to Be Famous
- We Are the Wave

I really enjoyed the miniseries in particular; it allows the filmmaker to tell a more detailed story, yet it doesn't require as much time (and commitment) to watch, as opposed to, for example, a 7-season-long show.

A movie feels short when you're used to a series. But that's part of the appeal I think. A movie doesn't try to suck you in, like a series does. It just tells a story.

<small>I wrote this a while back (somewhere fall last year), but I forgot to publish it. I found the draft yesterday. Also, a lot of movies listed above share one common theme: they're all semi-related to the reproductive system; I'm a teenager, after all ;)</small>
