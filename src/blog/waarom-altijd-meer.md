---
title: Waarom altijd meer?
date: 2023-04-26
favorite: true
---

Afgelopen jaar zat ik in de derde en moest ik mijn profielkeuze maken. Op het vwo werkt het zo dat je tot de vierde klas *alle* vakken krijgt, en daarna een profiel kiest met vakken waarin je examen gaat doen.

Ik zit nu in 4 atheneum en het profiel dat ik heb gekozen is NT (Natuur en Techniek). Dat betekent dat mijn vakkenpakket bestaat uit natuurkunde, scheikunde, NLT, Duits, Nederlands, Engels, informatica en wiskunde B. Volgens sommigen is dit het "moeilijkste" pakket.

Op de middelbare school zijn er vier profielen waar je in de bovenbouw uit kan kiezen: EM, CM, NT en NG. Doorgaans worden de N-profielen gezien als "zwaarder", omdat ze een bepaald analytisch denkniveau vereisen, en dat niet voor iedereen is weggelegd. CM wordt juist gezien als een "pretpakket", omdat je er geen wiskunde voor hoeft te doen. De "moeilijkheid" van een profiel is ook compleet relatief; voor mij zouden kunst en vreemde talen een veel grote uitdaging zijn dan wiskunde. Maar toch heerst er dit beeld dat NT het "beste" profiel zou zijn, en CM een "pretpakket".

Ook volgens mijn decaan. Vorig jaar kregen we van hem voorlichting over onze profielkeuze, waarin hij kort uitlegde wat elk profiel inhield. Hij benadrukte daarbij dat elk profiel gelijkwaardig is; een diploma met een N-profiel is niet meer of minder waard dan een diploma voor een M-profiel. Echter bleek vervolgens uit zijn hele verhaal dat hij CM maar onzin vond en het liefst ziet dat iedereen NT zou doen. (En het liefst geneeskunde of rechten als vervolgopleiding dan.)

Ik snap dat niet. Ik snap niet dat iedereen de hele tijd maar *meer* en *hoger* wil. Ik heb persoonlijk gekozen voor NT omdat natuurkunde mijn favoriete vak is en kunst me helemaal niet ligt. Niet omdat het schijnbaar het "beste" pakket voor de "slimste" leerlingen is. Een aantal van mijn vrienden hebben voor EM en CM gekozen, hoewel ik denk dat ze prima NG of NT aan zouden kunnen; of ze daar blij van zouden worden is echter een ander verhaal. Kortom, je zou het profiel moeten kiezen dat het best bij jou en je interesses past, niet het "hoogste" dat je aankan.

En ik zie nu hetzelfde weer gebeuren bij de voorlichting over onze studiekeuze. Want in principe leidt atheneum op tot *zowel* het <abbr title="Hoger beroepsonderwijs">HBO</abbr> als het <abbr title="Wetenschappelijk onderwijs">WO</abbr>. Maar in alle voorlichting wordt het HBO gepresenteerd als "afstromen", of een "tussenstap op de ladder naar de universiteit".

Maar wat als je nou helemaal geen ambitie hebt om naar de universiteit te gaan? De universiteit is heel theoretisch. De focus ligt vooral op leren onderzoeken, en je carriërepad na je afstuderen is vaak onduidelijk, terwijl je op de hogeschool juist echt *een vak* leert. Persoonlijk zou ik liever naar het HBO gaan om een docentenopleiding te volgen dan dat ik onderzoek zou gaan doen op de universiteit, en ik ken een aantal klasgenoten die daar exact hetzelfde in staan. De hogeschool is ook gewoon een "eindpunt", net als het wetenschappelijk onderwijs. Het is niet een alleen een "tussenstap" of "plan B".

Mijn zusje is nu ook aan haar profielkeuze toe. Net als ik zit zij op het vwo (nu in de derde). Haar eerste keuze was NG op het vwo. CM ligt haar ook wel, maar was geen optie omdat ze een hekel heeft aan Duits--een verplicht vak in CM op vwo-niveau.

Onze school biedt ook een speciaal vak genaamd BSM (Bewegen, Sport en Maatschappij) aan, waardoor je nu eindexamen kan doen in sport. Dit past heel goed bij Lize, want ze is van plan circusartiest of sportdocent te worden. Maar helaas is BSM uitsluitend te volgen op de havo.

Daarom heeft ze besloten over te stappen naar de havo en daar CM te doen met BSM als keuzevak. Ik denk dat dit een hele slimme zet is, omdat ze iets gaat doen waar ze blij van wordt en dat goed bij haar past. Maar in de ogen van onze geliefde decaan gaat ze van het één-na-"hoogste" pakket op vwo naar het "laagste" profiel een niveau lager: paniek! En hoewel ze meerdere keren heeft aangegeven dat ze geen ambitie heeft naar de universiteit te gaan en dat ze na haar eindexamen graag iets met sport wil gaan doen, probeert hij haar toch krampachtig over te halen op het vwo te blijven. Je wil toch het "hoogste" uit jezelf halen?

Nu is mijn vraag: Waarom altijd meer?
