---
title: Introduction
date: 2023-10-15
---

Hi. So, you stumbled upon my blog, cool.

I'm a random teenager from the Netherlands, and this is my little corner of the internet.

I post little notes about my life here. I made this site because I love minimalism and the initial idea of the web: a giant library of knowledge. This is my lil' corner of that library--as lightweight and simple as it can get. I'm not trying to scream; I'm just looking to talk.

This blog is mostly for myself, but you can read along if you're inclined. I'm planning to do mostly long-form casual writing. For now posts will be published exclusively on the Web, but I'm looking into Gemini and Gopher hosting.

I'm really bad at these “welcome!!” kind of posts. Sowwy!
