---
title: The RSS Club
date: 2024-05-26
rssonly: true
---

I'm setting up RSS-only posts on my site, inspired by Dave Rupert's [RSS Club](//daverupert.com/rss-club/), a 'secret society' consisting of individuals that publish some of their posts exlusively using the ancient peer-to-peer syndication protocol named [RSS](/rss).

If you're reading this, you've probably found this post via my RSS feed. Congratulations, you're considered a regular reader, and thus rewarded with extra postings now and then.

Of course, RSS-only postings can still be directly accessed if you know the URL. But they'll be unlisted on the index and in post archives. Public, but somewhat hidden. An open secret, if you will.

As with any club, there are some rules:

<blockquote href="https://daverupert.com/rss-club/">
  <ul>
    <li>1st rule of RSS Club is "Don't Talk About RSS Club"</li>
    <li>2nd rule of RSS Club is "Don't Share on Social Media"</li>
    <li>3rd rule of RSS Club is "Provide Value"</li>
  </ul>

  <p>Don't talk about it. Let people find it. Make it worthwhile.</p>
</blockquote>

RSS-only postings will generally consist of loose ideas, short updates and more personal posts. Things that I'd like to share, but not put "out there". Lil' easter eggs for RSS enthousiasts.
