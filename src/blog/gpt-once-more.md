---
title: Disconnect in the Education System
date: 2024-11-13
---

Yesterday, I published a rewritten version of my post on the role of generative AI in high school. I wanted to add one more thing:

> I think the rising popularity of ChatGPT for writing reports and 'research' logs raises questions about the methods used in our education system. Students en masse are resorting to AI tools to generate them, because frankly, writing reports is tedious, repetitive, and uninspiring; it completely drains the fun out of any course. And I seriously doubt teachers enjoy marking them either. The widespread use of LLMs for these kinds of assignments may not be the problem that needs to be fixed. It might just be a symptom of a larger issue with our education system: an obsession with measurable progress and attaining results.
>
> There is a deeper disconnect here–. These kinds of assignments fail to truly engage students to think about the subject, and focus on a measurable result instead. Furthermore, they completely drain all the fun out of the course. There is no room for creativity, exploration, and curiosity. We forgot that learning can be fun. No wonder students would rather have a computer do it.
