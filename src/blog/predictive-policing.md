---
title: Predictive policing
date: 2024-03-30
---

Last week we were reading an article about predictive policing at school. These are my two cents.

> In an era of tight budgets, police departments accross the country are being asked to do more with less. To address this problem, law-enforcement agencies are increasingly turning to data and analytics to improve their ability to fight crime without substantial increase in operating cost. Known as predictive policing, these technologies and techniques empower police officers to take a more proactive approach to both preventing crime and solving open cases.
>
> [T]he trend toward evidence-based [predictive] policing should ultimately enhance the relationship between communities and police officers. **That's because data-driven decision making is a step away from decisions based on biases that can result in unfair discrimination.**

Allow me to disagree. In computer science there's a principle of ['garbage in, garbage out'](//en.wikipedia.org/wiki/GIGO):

<blockquote cite="https://en.wikipedia.org/wiki/GIGO">
  <p>
    GIGO is the concept that flawed, biased or poor quality ("garbage")
    information or input produces a similar result or output.
  </p>
</blockquote>

Your predictive policing algorithm may be objective, but the data it's being trained on almost certainly isn't. So you've actually made it worse: decisions made by such an algorithm are as biased as decisions made by humans, but with the illusion of objectivity. You haven't eliminated bias; you've now institutionalised it.

<details>
  <summary>Extras</summary>

  <blockquote>
    <p>
      The use of data, like the use of any tool, leaves openings for
      misuse. There's a big difference, for example, between predicting
      where crime is most likely to occur and
      <strong>
        developing a list of potential future offenders without probable
        cause,
      </strong>
      a practice that certainly raises serious ethical and legal
      concerns.
    </p>
  </blockquote>

  <p>
    Just let me get this straight. You're talking about creating a list
    of suspects for <em>crimes not yet committed</em>. What happened to
    the principle of 'innocent until proven guilty'?
  </p>

  <p>
    While the article mentions <em>the possibility</em> of misuse of
    data for less ethical causes like this, it unfortunately doesn't
    explain how it's gonna <em>prevent</em> data being used for these
    purposes :shrug:
  </p>
</details>
