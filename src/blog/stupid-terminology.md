---
title: 'Stop saying "users" and "content"'
date: 2024-01-15
favorite: true
---

Terminology like *users* and *content* is demeaning and hurtful.

Artists aren't creating *content* to populate our datasets--they're creating movies, music, essays, drawings, games, paintings. Labeling a work that someone created, out of passion and love, *content*, is frankly disrespectful. It reduces the creative work from something beautiful to merely another piece of *content*, and greatly undervalues the effort and skill necessary to create it.

Likewise, calling people *users*, dehumanizes them. It turns them into a mere datapoint, or worse, an asset to be exploited.

<blockquote>
  <p>
    There are only two industries that call their customers "users":<br />
    illegal drugs and software.
  </p>

  --<cite>Edward Tufte</cite>
</blockquote>

Terms like these shift focus away from the humans in the equation. Language is the lense through which we see the world. Use it wisely.
