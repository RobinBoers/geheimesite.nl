---
title: love, hate
date: 2024-05-26
---

This lyric from Olivia O'Brien's song <cite>hate u love u</cite> summarizes my relationship with the Web almost perfectly:

<blockquote
  cite="//open.spotify.com/track/6ATgDc6e4sPn84hvJsAmPt">
  <p>
    I hate you, I love you<br />
    I hate that I love you
  </p>
  <p>
    Don't want to, but I can't put<br />
    nobody else above you
  </p>
</blockquote>

Cause, as a developer, I lóve the Web. The web browser is a blank canvas that can be used to build virtually anything. HTML, CSS, Javascript--the languages used to build websites--are easy to grasp but at the same time extremely powerful and versatile. There are APIs for literally anything I'd want to build. And unlike low-level languages such as C and Rust, a website doesn't make me to deal with a complicated toolchain; I don't need to think about complex cross-compilation, memory allocation, dynamic linkers or glibc. I simply upload my HTML files to my server using `rsync`, and it automagically Just Works(TM).

However, as a consumer, I absolutely detest the Web. It feels like it's turned from a wild, collaborative, global creative space into something akin to a shopping mall. It's just [straight up depressing](//how-i-experience-web-today.com).

Everyone's talking about *monetization*[^1], *growth hacking*, *ad-revenue*. Everywhere I go, I'm met with pages full of plausible sounding AI-generated SEO-optimized nonsense. I get asked to accept these *delicious* cookies, to sign up for the newsletter, and to create a (*free!!*) account. Also: *this content is better in the app*! And we're *announcing our next-gen AI-powered blockchain startup, check it out!* All while some completely unrelated video starts autoplaying in the background. And that's without even talking about the ads.

These websites are not built for you *to read*, they are designed for *extracting value from your visit*. Desperately trying to squeeze every last drop of data out of you, before youinevitably close the tab.

[^1]: Note about monetization: It's not that I think creative people shouldn't be fairly compensated. Of course they should. But this isn't about them. This is about a cultural shift that turns the Web into a business platform. If you make something--art, music, film--and put it online for others to see, but also request to be compensated for your work, that's totally fair. But what we're seeing now is companies hijacking the Web, replacing genuine people writing making real things, with SEO-optimized clickbait meant to make money quick.
