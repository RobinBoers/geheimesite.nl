---
title: Somtoday verbeteren met CSS
date: 2022-03-25
language: nl
---

Kennen jullie Somtoday? Voor de mensen die het niet kennen: het is een leerlingvolgsysteem voor het voortgezet onderwijs. En het is toevallig ook wat mijn school, het Lyceum Schravenlant, gebruikt.

Som is noice. Je kan er je cijfers, huiswerk en leermiddelen zien. Maar ik zie ook een aantal verbeterpuntjes. Somtoday heeft voor de docenten een uitgebreid dashboard waarin ze het huiswerk kunnen inplannen, cijfers kunnen invoeren, absenties kunnen bijhouden etc.

<figure>
  <img src="https://cdn.geheimesite.nl/images/blog/somtoday-docent.png" alt="">
  <figcaption>Somtoday Docent: een mooie moderne Angular app</figcaption>
</figure>

Maar leerlingen en ouders zien deze website uit het jaar 1900. Oke, dat is misschien een beetje overdreven, maar het doet me wel serieus denken aan websites uit 2000.

<figure>
  <img src="https://cdn.geheimesite.nl/images/blog/somtoday-leerling.png" alt="">
  <figcaption>Somtoday voor leerlingen en ouders: een site die eruitziet alsof hij uit een tijdmachine komt</figcaption>
</figure>

Er zijn heel veel dingen die me irriteren aan dit ontwerp. Om er een paar te noemen:

- De kleuren zijn verschrikkelijk
- De font-size is niet ideaal
- Mijn lelijke hoofd staat bovenaan
- Er is permanent een badge met het nummer 3 bij de berichtenbox, ook al het ik al mijn berichten gelezen

Zoals mijn lezers waarschijnlijk weten ben ik een webdesign freak die weet hoe CSS werkt. Dus heb ik een userstyle geschreven die je kan installeren om het leerling/ouderpaneel iets beter te maken. Het is gebaseerd op de look van Somtoday Docent, en ziet er zo uit:

<figure>
  <img src="https://cdn.geheimesite.nl/images/blog/somtoday-verbeterd.png" alt="">
  <figcaption>Verbeterde versie van Somtoday voor leerlingen en ouders</figcaption>
</figure>

Als je wil kan je mijn userstyle [op mijn GitHub account](//gist.github.com/RobinBoers/c95ea8c4181bcfcd9957bf730effecbb) vinden.

Userstyles kan je installeren met [Stylus](//addons.mozilla.org/en-US/firefox/addon/styl-us/). Je kan in de extension mijn CSS code plakken en als het goed is zou dan je Somtoday er veel beter uit moeten zien :)

**Ik hoop stiekem heel erg dat een medewerker van Somtoday dit toevallig leest. Zo ja, zou je me in contact willen brengen met de designers / ontwikkelaars van Som? Mijn emailadres is staat op de homepage :)**
