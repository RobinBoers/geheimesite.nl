---
title: What 19th-century literature can teach us about modern morality and ethics
layout: blank
head: >
    <style>
      html {
        font-family: sans-serif;
        font-size: 1rem;
        line-height: 1.5;
        color-scheme: light dark;
      }
      body {
        max-width: 75ch;
        border-radius: 10px;
        padding: 2em;
        margin: 10px auto;
      }
    </style>
---

<h1>{{ title }}</h1>

The story of <cite>Desirée's Baby</cite> (Chopin, 1893) is a story about
how a father discovered that his son and possibly his wife weren't
completely white, and thus didn't want anything to do with them anymore.
The story portrays important issues, such as slavery, racism, and lack of
empathy. Issues we seem to have solved. Or have we? Was social class that
much more important compared to moral values, in the 19th century, than it
is now?

We are usually quick to judge the past. If we look at the behaviour of the
characters through the lens of our contemporary culture and
beliefs--our collective moral code so to say--it may seem
immoral or irrational. In modern society, slavery and racism--and
therefore by extent Armand's behaviour--are condemned. He chose not
to love, and actually abandon, his wife and newborn son for the fact that
they were said to belong to a different (non-white) race, and the
implications it may have had for his reputation and status. Additionally,
his mother was so ashamed by the fact that she belonged to the 'race that
is cursed with the brand of slavery', that she hid her ancestry from her
son for his entire life. A rationally thinking human being wouldn't
degrade themselves like that.

However, if we look at the historical context and the environment the
characters live in, it could better explain their actions. They've grown
up in a society where slavery and racism are normal and accepted, and have
been repeatedly told--conditioned, indoctrinated even--that
black people are somehow of a different "race" that is inherently bad and
evil. Can we blame them for their actions given this context?

We might think we are a lot more humane and empathic in the 21st century
than we were two centuries ago, however, in many aspects we still place
social class above moral values. Take designer clothes: we somehow assume
we have the "right" to cheap clothes, even though we know those clothes
are most likely being produced by leveraging Chinese child labour. The
same goes for forced labour being used to produce products for cheap sites
like Temu. We might pet ourselves on the shoulder for abolishing slavery,
but we've actually just outsourced slavery to companies abroad, therefore
shifting the blame. Out of sight, out of mind.

Thus, you might argue that our behaviour in the 21st century is actually
worse, since we have been raised with the notion that racism and slavery
are bad and unacceptable. Knowing this, and still accepting or even
ignoring the existence of modern slavery may be even more problematic.
Another example would be how climate change and the effect of our actions
on the environment are not considered or outright disregarded. This
subject is taught in schools worldwide--not to mention the amount of
campaigns dedicated to awareness--, yet people still ignore the
effects of their actions and continue to choose comfort and status over
the morally right thing to do.

In conclusion, given the historical context, the characters' behaviour in
<cite>Desirée's Baby</cite> isn't as irrational as we believe it is. They
place social class above moral values because they have been conditioned
to do so. In the 21st century however, we have taken pride in abolishing
slavery, while we have in fact externalised it to distant places. We've
grown up in a society where slavery is unacceptable, nevertheless modern
slavery is disregarded and overlooked, and the same applies to the
environment. While we've certainly progressed since the days of
<cite>Desirée's Baby</cite>, sadly, we have not yet reached the moral
highs we aspire to.

<footer>
  <p style="text-align: right">
    <small><i>Written by Robin Boers and Lize Boers.</i></small>
  </p>

  <h4>References</h4>

  <p>Chopin, K. (1893, January 14). Desirée's Baby. <i>Vogue</i>.</p>
</footer>
