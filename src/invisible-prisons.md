---
title: Invisible Prisons
layout: blank
favicon: 🕊️
head: >
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Exo:wght@100..900&family=Josefin+Slab:wght@100..700&family=Maitree&display=swap">

  <style>
    html {
      color: aliceblue;
      background: linear-gradient(114deg, rgb(1, 0, 23) 0%, rgba(0,60,83,1) 58%, rgb(13, 0, 98) 100%);
      background-attachment: fixed;
      font-family: 'Maitree', 'Times New Roman', Times, serif;
      font-size: 1rem;
      line-height: 1.5;
    }

    body {
      max-width: 700px;
      width: 100%;
      padding: 1em;
      margin: 0 auto;
      padding-right: 600px;
      font-size: 1em;
      overflow-x: hidden;
    }

    p {
      font-size: 1.1em;
    }

    h1 {
      font-family: "Exo", monospace;
      font-style: normal;
      margin-top: 2em;
    }

    blockquote {
      font-family: "Exo", monospace;
      font-style: italic;
      font-size: 1.3em;
      margin-top: 1.5em;
      margin-bottom: 2em;
      margin-left: -1em;
    }

    @media (min-width: 1222px) {
      .bg { display: block }

      blockquote {
        position: fixed;
        bottom: 50px;
        right: 20px;
        max-width: 600px;
      }
    }

    .bg-img {
      position: fixed;
      opacity: 0.8;
      top: 0;
      right: -25vw;
      background-color: transparent;
      mix-blend-mode: hard-light;
      width: 100%;
      height: 100%;
      object-fit: cover;
      mask-image: linear-gradient(to right, transparent, transparent, white, white, white);
      mask-size: cover;
      pointer-events: none;
    }
  </style>
---

<div class="bg" hidden>
  <img class="bg-img" src="https://cdn.geheimesite.nl/images/bird.webp" alt="">
</div>

<h1>{{ title }}</h1>

The shrieking sound of mechanical brakes echoes in the great tubes of the London Underground as the 0-18 arrives at Little Park Road. Inside, a dozen not-quite-birds are sitting on worn-out green benches, their faces illuminated by the blue light emitted by their shiny glass slabs. Routinely, a cold robotic voice announces a barely decipherable string of seemingly random syllables, and a few of the featherless animals get up. Their fellow passengers don't even seem to notice; all of them entirely absorbed by the glass slabs anxiously clamped in their hands, riding the waves of dopamine. Desperate for a temporary escape from their self-imposed misery, imperceptive of their surroundings.

Amidst them, a young man is sitting; his face not covered by the soft blue glow of contemporary tragedy, but rather intently staring at a bundle of paper inscribed with incomprehensible symbols. The green spine looks weirdly turquoise in the bleak artificial lighting of the fluorescent tubes above.

Opposite of him, a moderately attractive girl is inattentively swiping on her respective glossy rectangle. She's feeling restless, but can't quite place why. She swipes again; moving colored blobs back and forth in what was marketed as a highly engaging game of Candy Crush, but turned out to be a tiresome and frankly quite dull activity. Dissatisfied, she opens Instagram--which happened to be closest to her thumb after discarding Candy Crush--and begins to scroll; still searching for that dopamine hit she so hopelessly craves.

A high-pitched cracking comes out of the speakers of the cabin as the carriage comes to an abrupt halt. The robotic voice again echoes through the somber metro. Park Avenue. Again, no one really seems to notice. The young man gets up and makes his way to the doors. This is his stop.

It just so happens that, as the train lurched to a stop, the young girl lost her balance. For a moment, her grip on the shiny rectangle slips. With a dull thud it lands on the stained gray floors of the wagon.

Confused, she takes in her surroundings. She feels like waking from a dream. Reality sharpens. Around her, dozens of her fellow wingless creatures are mindlessly gazing at the glass slabs clammed in their hands. Looking around, a feeling of unease comes creeping in. Their eyes are vacant, lost in the endless stream of <em>mediocre</em>. Her own rectangle, once glowing with the promise of that sweet, sweet dopamine, now appears rather lifeless and stale. Laying on the floor, it has lost its shine.

She just catches a glimpse of the young man's back, and then the doors close. He looked quite handsome, she thinks to herself. Looking around once more, she wonders, are these poor creatures happy? Are they living, or are they just merely existing; discontent but unable to articulate why?

Pingg! The glass slab on the ground lights up. Instantly, her synapses begin firing again, every thought succumbed by an overpowering rush of dopamine. The brief moment of clarity is gone. She's back to swiping bleak, formless blobs. Occupied with her phone, she doesn't notice the note the young man left for her.

The metro gains speed again. The omniscient bird silently watches it run away. A few seconds later, it's gone.

> Their cages aren't built of iron, but of glass. Prisons whose bars they cannot see.

<footer>
  <p style="text-align: right">
    <small><i>Written in despair, misery, tragedy and heartbreak. Forgotten in vain.</i></small>
  </p>
</footer>
